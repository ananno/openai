# OpenAI
This is my journey to learn OpenAI. Using `gym` from `openai` I'm trying to learn Reinforcement Learning.

## Backend
* [OpenAI](https://www.openai.com/) : [Github](https://github.com/openai)
  * [Gym](https://github.com/openai/gym)
  
## Repositories
* [MountainCar](/mountaincar)
