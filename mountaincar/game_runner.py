import gym
import math
import numpy as np
import tensorflow as tf

import random
import matplotlib.pyplot as plt

from model import Model
from memory import Memory


BATCH_SIZE = 100
MEM_SIZE = 50000
MIN_EPSILON = 1e-7
MAX_EPSILON = 1.0
LAMBDA = 1e-6
GAMMA = 0.001


class GameRunner:
    def __init__(self, sess, model, env, memory, max_eps, min_eps, decay, render=True):
        self._sess = sess
        self._env = env
        self._model = model
        self._memory = memory
        self._render = render
        self._max_eps = max_eps
        self._min_eps = min_eps
        self._decay = decay
        self._eps = self._max_eps
        self._steps = 0
        self._reward_store = []
        self._max_x_store = []

    def run(self):
        state = self._env.reset()
        tot_reward = 0
        max_x = -100
        while True:
            if self._render:
                self._env.render()

            action = self._choose_action(state)
            next_state, reward, done, info = self._env.step(action)
            if next_state[0] >= 0.1:
                reward += 10
            elif next_state[0] >= 0.25:
                reward += 20
            elif next_state[0] >= 0.5:
                reward += 100

            if next_state[0] > max_x:
                max_x = next_state[0]

            if done:
                next_state = None
            else:
                self._memory.add_sample((state, action, reward, next_state))
            self._replay()

            self._steps += 1
            self._eps = MIN_EPSILON + (MAX_EPSILON - MIN_EPSILON) * math.exp(-LAMBDA * self._steps)

            state = next_state
            tot_reward += reward

            if done:
                self._reward_store.append(tot_reward)
                self._max_x_store.append(max_x)
                break

        print("Step {}, Total reward: {}, Eps: {}". format(self._steps, tot_reward, self._eps))

    def _choose_action(self, state):
        if random.random() < self._eps:
            return random.randint(0, self._model._num_actions -1)
        else:
            return np.argmax(self._model.predict_one(state, self._sess))

    def _replay(self):
        batch = self._memory.sample(self._model._batch_size)
        states = np.array([val[0] for val in batch])
        next_states = []
        for val in batch:
            if val[3] is None:
                next_states.append(self._model._num_states)
            else:
                next_states.append(val[3])
        next_states = np.asarray(next_states)

        q_s_a = self._model.predict_batch(states, self._sess)
        q_s_a_d = self._model.predict_batch(next_states, self._sess)

        x = np.zeros((len(batch), self._model._num_states))
        y = np.zeros((len(batch), self._model._num_actions))

        for i, b in enumerate(batch):
            state, action, reward, next_state = b[0], b[1], b[2], b[3]

            current_q = q_s_a[i]

            if next_state is None:
                current_q[action] = reward
            else:
                current_q[action] = reward + GAMMA * np.amax(q_s_a_d[i])

            x[i] = state
            y[i] = current_q

        self._model.train_batch(self._sess, x, y)


if __name__ == '__main__':
    env_name = "MountainCar-v0"
    env = gym.make(env_name)

    num_states = env.env.observation_space.shape[0]
    num_actions = env.env.action_space.n

    model = Model(num_states, num_actions, BATCH_SIZE)
    mem = Memory(MEM_SIZE)

    with tf.Session() as sess:
        sess.run(model._var_init)
        gr = GameRunner(sess, model, env, mem, MAX_EPSILON, MIN_EPSILON, LAMBDA)
        num_episodes = 300
        cnt = 0
        while cnt < num_episodes:
            if cnt % 10 == 0:
                print("Episode {} of {}".format(cnt+1, num_episodes))
            gr.run()
            cnt += 1
        plt.plot(gr._reward_store)
        plt.show()
        plt.close("all")
        plt.plot(gr._max_x_store)
        plt.show()
